export function teamConfirmed(team,teamMembers){
  if(team == 'Host') {
    return {
      type: "HOST_TEAM_CONFIRMED",
      teamMembers
    }
  } else {
    return {
      type: "GUEST_TEAM_CONFIRMED",
      teamMembers
    }
  }
}
