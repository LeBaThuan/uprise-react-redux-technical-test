import { createStore, compose, applyMiddleware} from 'redux';
import { syncHistoryWithStore,routerMiddleware } from 'react-router-redux';
import { browserHistory } from 'react-router';
import * as thunk  from 'redux-thunk';
import * as promise from 'redux-promise-middleware';
import * as logger from 'redux-logger';
import rootReducer from './reducers/index';

const middleWare = applyMiddleware(thunk,promise(),logger,routerMiddleware(browserHistory));
const composeEnhancers = compose;
const defaultState = sessionStorage.getItem('reduxState') ? JSON.parse(sessionStorage.getItem('reduxState')) : {};
const store = createStore(rootReducer, defaultState, composeEnhancers(middleWare));

export const history = syncHistoryWithStore(browserHistory,store);

export default store;
